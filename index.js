// console.log("Hello World");

let number = Number(prompt("Enter a Number:"));
console.log("The number you provided is: " + number);
for(i = number; i>0; number--){
    if(number <= 50){
        console.log("The current number is " +number + " . Terminating the loop")
        break;
    }
    if((number%10) == 0){
        console.log("The number is divisible by 10. Skipping the number");
        continue;
    }
    if((number%5) == 0){
        console.log(number);
        continue;
    }
}

let text = "supercalifragilisticexpialidocious";
let display = "";
console.log(text);
for(t=0; t < text.length; t++){
    if(text[t].toLowerCase() == "a" || text[t].toLowerCase() == "e" || text[t].toLowerCase() == "i" || text[t].toLowerCase() == "o" || text[t].toLowerCase() == "u"){
        continue;
    }
    else{
        display += text[t];
    }
}
console.log(display);